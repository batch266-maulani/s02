import java.util.ArrayList;
import java.util.HashMap;

public class activity {
    public static void main(String[] args) {
        int prime[] = {2,3,5,7,11};

        System.out.println("The first prime number is: " + prime[0]);


        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String,Integer> tooth = new HashMap<String,Integer>();

        tooth.put("toothpaste",15);
        tooth.put("toothbrush",20);
        tooth.put("soap",12);

        System.out.println("Our current inventory consists of: " + tooth.toString());


    }
}
