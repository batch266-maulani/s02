package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;

public class Array {

    public static void main(String[] args) {

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;

        System.out.println(Arrays.toString(intArray));

        String[] names = {"John","Jane","Joe"};
        System.out.println(Arrays.toString(names));


        Arrays.sort(intArray);
        System.out.println(Arrays.toString(intArray));

        String[][] classroom = new String[3][3];

        //first row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";


        //third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        System.out.println(Arrays.deepToString(classroom));


        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane","Mike"));

        students.add("John");
        students.add("Paul");

        System.out.println(students);

        students.add(0,"Joey");

        System.out.println(students);

        students.set(0,"George");

        System.out.println(students);

        students.remove(1);

        System.out.println(students);
        System.out.println(students.size());

        students.clear();

        System.out.println(students);

        System.out.println(students.size());

    }


}
